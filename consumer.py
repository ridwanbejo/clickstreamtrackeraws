import boto3
import time
from elasticsearch import Elasticsearch
from uuid import uuid4
import json

# Get the service resource
sqs = boto3.resource('sqs')
es = Elasticsearch()

# Get the queue
queue = sqs.get_queue_by_name(QueueName='clickstreamQueueDev')

while True:
	# Process messages by printing out body and optional author name
	for message in queue.receive_messages(MessageAttributeNames=['Author']):
	    # Get the custom author message attribute if it was set
		author_text = ''
		if message.message_attributes is not None:
		    author_name = message.message_attributes.get('Author').get('StringValue')
		    if author_name:
		        author_text = ' ({0})'.format(author_name)

		# Print out the body and author (if set)
		print('Receive: {0} from {1}'.format(message.body, author_text))

		payload = json.loads(message.body)

		doc = { 
			'userId': payload['userId'], 
			'mouse_position_x': payload['mouse_position_x'], 
			'mouse_position_y': payload['mouse_position_y'],
			'sourceUrl': payload['sourceUrl'], 
			'createdAt':"2017-01-01"
		}

		res = es.index(index="mylog", doc_type='clickstreamLog', id=str(uuid4()), body=doc)

		# Let the queue know that the message is processed
		message.delete()

	print("Still running!")
	time.sleep(2)