// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');

// Set the region
AWS.config.update({region: 'ap-northeast-1'});

// Create SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});
var queueURL = "https://sqs.ap-northeast-1.amazonaws.com/996680382794/clickstreamQueueDev";

var params = {
   AttributeNames: [
      "SentTimestamp"
   ],
   MaxNumberOfMessages: 1,
   MessageAttributeNames: [
      "All"
   ],
   QueueUrl: queueURL,
   VisibilityTimeout: 0,
   WaitTimeSeconds: 0
};


// configure the elasticsearch

var elasticsearch = require("elasticsearch");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    log: "trace"
});

const uuidV4 = require('uuid/v4');

for (var i = 0; i < 5; i++)
{
    setTimeout(function(){
      sqs.receiveMessage(params, function(err, data) {
            if (err) {
              console.log("Receive Error", err);
            } else if (data.Messages) {

              console.log(data.Messages.length);

              var log = JSON.parse(data.Messages[0].Body);

              client.index({  
                index: 'mylog',
                id: uuidV4(),
                type: "clickstreamLog",
                body: {
                    mouse_position_x: log.mouse_position_x,
                    mouse_position_y: log.mouse_position_y,
                    sourceUrl: log.sourceUrl,
                    userId: log.userId,
                    createdAt: "2017-02-05"
                }
                },function(err,resp,status) {
                  if(err) {
                    console.log(err);
                  }
                  else {
                    console.log("create",resp);
                  }
              });
                
              var deleteParams = {
                QueueUrl: queueURL,
                ReceiptHandle: data.Messages[0].ReceiptHandle
              };

              // deleting processed message
              sqs.deleteMessage(deleteParams, function(err, data) {
                if (err) {
                  console.log("Delete Error", err);
                } else {
                  console.log("Message Deleted", data);
                }
              });
            
            }
      });

    }, 1000);
}

