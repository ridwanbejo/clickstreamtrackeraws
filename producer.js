var QUEUE_URL = 'https://sqs.ap-northeast-1.amazonaws.com/996680382794/clickstreamQueueDev';
var AWS = require('aws-sdk');
var sqs = new AWS.SQS({region : 'ap-northeast-1'});

exports.handler = function(event, context, callback) {
    var params = {
        MessageBody: event.body,
        QueueUrl: QUEUE_URL
    };
    
    sqs.sendMessage(params, function(err, data){
        if(err) {
          console.log('error:',"Fail Send Message" + err);
          callback(null, { statusCode: 400, body: '{"message":"Failed to queue the message!"}' });
        }else{
          console.log('data:',data.MessageId);
          callback(null, { statusCode: 200, body: '{"message":"Success!"}' });
        }
    });
};
